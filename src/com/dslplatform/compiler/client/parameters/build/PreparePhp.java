package com.dslplatform.compiler.client.parameters.build;

import com.dslplatform.compiler.client.*;

import java.io.*;
import java.util.Scanner;

public class PreparePhp implements BuildAction {

	private static final String CACHE_NAME = "php_target_folder";

	private static final String SOURCES_LINE_SEPARATOR = "\r\n";

	@Override
	public boolean check(final Context context) throws ExitException {
		final String customFolder = context.get("php");
		final File target = new File(customFolder != null ? customFolder : "php/src");
		if (target.exists() && target.isDirectory()) {
			try {
				Utils.deletePath(target);
			} catch (IOException ex) {
				context.error("Failed to clean PHP target folder: " + target.getAbsolutePath());
				context.error(ex);
				throw new ExitException();
			}
		} else if (target.exists() && target.isFile()) {
			if (!target.delete()) {
				context.error("Failed to delete PHP sources archive: " + target.getAbsolutePath());
				throw new ExitException();
			}
		} else if (target.getName().endsWith(".php")) {
			File parent = new File(target.getParent());
			if (!parent.exists() && !parent.mkdirs()) {
				context.error("Failed to create PHP archive target folder: " + parent.getAbsolutePath());
				throw new ExitException();
			}
		}
		else if (!target.mkdirs()) {
			context.error("Failed to create PHP target folder: " + target.getAbsolutePath());
			throw new ExitException();
		}
		context.cache(CACHE_NAME, target);
		return true;
	}

	public static String mergeSources(final File sources, final Context context) throws ExitException {
		String mergedPhp = "<?php" + SOURCES_LINE_SEPARATOR;
		for (final String fn : sources.list()) {
			final File sf = new File(sources, fn);
			final Either<String> content = Utils.readFile(sf);
			if (!content.isSuccess()) {
				context.error("Error reading source PHP file: " + sf.getAbsolutePath());
				throw new ExitException();
			}
			Scanner sc = new Scanner(content.get());
			while (sc.hasNext()) {
				String line = sc.nextLine();
				if (line.startsWith("require_once") || line.startsWith("<?php"))
					continue;
				mergedPhp += line + SOURCES_LINE_SEPARATOR;
			}
		}
		return mergedPhp;
	}

	private static void copySourcesToSingle(final File sources, final File target, final Context context) throws ExitException {
		String mergedPhp = mergeSources(sources, context);
		try {
			Utils.saveFile(target, mergedPhp);
		} catch (IOException ex) {
			context.error("Error writing target PHP file: " + target.getAbsolutePath());
			throw new ExitException();
		}
	}

	private static void copySources(final File sources, final File target, final Context context) throws ExitException {
		for (final String fn : sources.list()) {
			final File sf = new File(sources, fn);
			final File tf = new File(target, fn.replace('\\', File.separatorChar));
			if (sf.isDirectory()) {
				if (!tf.mkdirs()) {
					context.error("Failed to create target PHP folder: " + tf.getAbsolutePath());
					throw new ExitException();
				}
			} else {
				final Either<String> content = Utils.readFile(sf);
				if (!content.isSuccess()) {
					context.error("Error reading source PHP file: " + sf.getAbsolutePath());
					throw new ExitException();
				}
				File parent = new File(tf.getParent());
				if (!parent.exists())
					parent.mkdirs();
				try {
					Utils.saveFile(tf, content.get());
				} catch (IOException ex) {
					context.error("Error writing target PHP file: " + tf.getAbsolutePath());
					throw new ExitException();
				}
			}
		}
	}

	private static void copyFolder(final File sources, final File target, final Context context) throws ExitException {
		if(target.getName().endsWith(".php"))
			copySourcesToSingle(sources, target, context);
		else
			copySources(sources, target, context);
	}

	@Override
	public void build(final File sources, final Context context) throws ExitException {
		final File target = context.load(CACHE_NAME);
		copyFolder(sources, target, context);
	}
}
